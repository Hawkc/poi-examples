package com.exampls.word.docx;

import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.*;
import java.io.*;
import static com.exampls.word.constant.CommonConstant.OUTPUT_DESKTOP_PATH;

public class Docx2PDF {
    public static void main(String[] args) {
        try {
            // 1) Load DOCX into XWPFDocument
            XWPFDocument document = new XWPFDocument(new FileInputStream(OUTPUT_DESKTOP_PATH + "recognize_template.docx"));
            document.createStyles();

            // 3) 测试是否可以打印表格
            CreatTableCase.createTableOROCPrintable(document);

            // 2) Prepare Pdf options
            PdfOptions options = PdfOptions.create();

            // 3) Convert XWPFDocument to Pdf
            OutputStream out = new FileOutputStream(new File(OUTPUT_DESKTOP_PATH + "HelloWord.pdf"));
            PdfConverter.getInstance().convert(document, out, options);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}