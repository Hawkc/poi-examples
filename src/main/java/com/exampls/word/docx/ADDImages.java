package com.exampls.word.docx;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ADDImages {

    private ADDImages() {}

    public static void addImages(XWPFRun r, String... imagePath) {
        try  {
            for (String imgFile : imagePath) {
                int format;
                if (imgFile.endsWith(".emf")) {
                    format = Document.PICTURE_TYPE_EMF;
                } else if (imgFile.endsWith(".wmf")) {
                    format = Document.PICTURE_TYPE_WMF;
                } else if (imgFile.endsWith(".pict")) {
                    format = Document.PICTURE_TYPE_PICT;
                } else if (imgFile.endsWith(".jpeg") || imgFile.endsWith(".jpg")) {
                    format = Document.PICTURE_TYPE_JPEG;
                } else if (imgFile.endsWith(".png")) {
                    format = Document.PICTURE_TYPE_PNG;
                } else if (imgFile.endsWith(".dib")) {
                    format = Document.PICTURE_TYPE_DIB;
                } else if (imgFile.endsWith(".gif")) {
                    format = Document.PICTURE_TYPE_GIF;
                } else if (imgFile.endsWith(".tiff")) {
                    format = Document.PICTURE_TYPE_TIFF;
                } else if (imgFile.endsWith(".eps")) {
                    format = Document.PICTURE_TYPE_EPS;
                } else if (imgFile.endsWith(".bmp")) {
                    format = Document.PICTURE_TYPE_BMP;
                } else if (imgFile.endsWith(".wpg")) {
                    format = Document.PICTURE_TYPE_WPG;
                } else {
                    System.err.println("Unsupported picture: " + imgFile +
                            ". Expected emf|wmf|pict|jpeg|png|dib|gif|tiff|eps|bmp|wpg");
                    continue;
                }

                try (FileInputStream is = new FileInputStream(imgFile)) {
                    r.addPicture(is, format, imgFile, Units.toEMU(200), Units.toEMU(200)); // 200x200 pixels
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }
}
